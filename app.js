const app = Vue.createApp({
    data: function () {
        return {
            name: 'Corentin',
            age: 27,
            imgGoogle: 'https://pbs.twimg.com/media/E-MQ5DeWQAMy0EZ.jpg'
        }
    },
    methods: {
        addFive() {
            return this.age + 5;
        },
        randbin() {
            return Math.random();
        }
    }
});

app.mount('#assignment');